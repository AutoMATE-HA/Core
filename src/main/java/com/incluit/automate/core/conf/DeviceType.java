package com.incluit.automate.core.conf;

public enum DeviceType {

    NO_TYPE(""), ANDROID("Android"), WINDOWS_WEB("windows.web");

    private String packageName;

    DeviceType(String packageName) {
        this.packageName = packageName;
    }

    public static DeviceType getDeviceTypeByPackage(String packageName) {
        for (int i = 0; i < DeviceType.values().length; i++) {
            if (DeviceType.values()[i].getPackage().equals(packageName)) {
                return DeviceType.values()[i];
            }
        }
        return null;
    }

    public String getPackage() {

        return this.packageName;
    }

}
