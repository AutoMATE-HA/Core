package com.incluit.automate.core.device.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermissions;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import java.nio.file.Path;

import com.incluit.automate.core.agent.Agent;
import com.incluit.automate.core.app.Application;
import com.incluit.automate.core.conf.Constants;
import com.incluit.automate.core.conf.Parameters;
import com.incluit.automate.core.conf.Constants.AppType;
import com.incluit.automate.core.conf.PropertiesKeys;
import com.incluit.automate.core.device.Device;
import com.incluit.automate.core.device.DeviceBase;
import com.incluit.automate.core.device.DeviceManager;
import com.incluit.automate.core.device.Utils;
import com.incluit.automate.core.exceptions.AgentException;
import com.incluit.automate.core.exceptions.DeviceException;
import com.incluit.automate.core.utils.ReadProperty;

public class SaucelabsDeviceImpl extends DeviceBase implements Device {

    private Agent agent;

    /*
     * Util for windows
     */
    private Utils utils = null;

    /**
     * Logger object
     */
    private static Logger log = Logger.getLogger(SaucelabsDeviceImpl.class
            .getName());

    /*
     * Device name
     */
    private String name;

    private String basePath = System.getProperty("user.dir") + File.separator;

    /**
     * Default constructor
     *
     * @param utils
     *            of the SO
     */
    public SaucelabsDeviceImpl(Utils utils, String name) {
        this.utils = utils;
        this.name = name;
    }

    @Override
    public void close() throws DeviceException {
        agent.close();
    }

    @Override
    public Application getApplication(String appKey, String isABrowser)
            throws DeviceException {
        appKey = (appKey == null) ? System
                .getProperty(Parameters.RUNNER_PARAMETER_APPLICATION_NAME)
                : appKey;

        try {
            installApp();
        } catch (Exception e1) {
            throw new DeviceException(
                    "Can not install the application in Sauce Labs");
        }

        try {
            if (!applications.containsKey(appKey)) {
                applications.put(appKey, new Application(appKey, isABrowser,
                        this));
            }
        } catch (AgentException e) {
            throw new DeviceException(e);
        }
        return applications.get(appKey);
    }

    private void installApp() throws Exception {

        if (!ReadProperty.isPropertyExist(PropertiesKeys.APK_MD5_KEY)) {
            saveKey(PropertiesKeys.APK_MD5_KEY, getMd5());
            installApk();
        } else {
            if (!getMd5().equals(
                    ReadProperty.getProperty(PropertiesKeys.APK_MD5_KEY))) {
                log.info("Exist a new APK version for application '"
                        + ReadProperty.getProperty(PropertiesKeys.APK_NAME_KEY
                                ) + "'");
                saveKey(PropertiesKeys.APK_MD5_KEY, getMd5());
                installApk();
            }
        }
    }

    private void saveKey(String key, String value) throws Exception {
        PropertiesConfiguration config = new PropertiesConfiguration();
        config.load(basePath + Constants.INFO_PROPERTIES_PATH);
        config.setProperty(key, value);
        config.save(basePath + Constants.INFO_PROPERTIES_PATH);
    }

    private String getMd5() throws Exception {
        FileInputStream fis = new FileInputStream(new File(basePath
                + String.format(Constants.APK_PATH_TEMPLATE,
                        ReadProperty.getProperty(PropertiesKeys.APK_NAME_KEY))));
        String md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(fis);
        fis.close();
        return md5;
    }

    private void installApk() throws Exception {

        File sh = new File(basePath + Constants.INSTALL_APK_SAUCE_LAB_SH_NAME);

        FileUtils.writeStringToFile(sh,
                Constants.INSTALL_APK_SAUCE_LAB_SH_CONTENT);
        Files.setPosixFilePermissions(
                Paths.get(sh.getAbsolutePath()),
                PosixFilePermissions
                        .fromString(Constants.INSTALL_APK_SAUCE_LAB_SH_PERMISSION));

        ProcessBuilder pb = new ProcessBuilder(
                "./" + Constants.INSTALL_APK_SAUCE_LAB_SH_NAME,
                ReadProperty.getProperty(PropertiesKeys.SAUCE_LABS_USER_NAME),
                ReadProperty
                        .getProperty(PropertiesKeys.SAUCE_LABS_AUTHETICARION_KEY),
                basePath
                        + String.format(
                                Constants.APK_PATH_TEMPLATE,
                                ReadProperty
                                        .getProperty(PropertiesKeys.APK_NAME_KEY)));
        pb.directory(new File(basePath));
        Process p;
        p = pb.start();

        BufferedReader reader = new BufferedReader(new InputStreamReader(
                p.getInputStream()));
        String s;
        while ((s = reader.readLine()) != null) {
            saveKey(PropertiesKeys.APP_KEY_KEY, s);
        }
    }

    @Override
    public boolean killApplication(String processName) throws DeviceException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean processIsRunner(String processName) throws DeviceException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Agent createAgent(AppType type, String agentImplPackageTemplate)
            throws DeviceException {
        String platform = this.getClass().getSimpleName()
                .replace("DeviceImpl", "").toLowerCase();
        agent = super.createAgent(type, platform, agentImplPackageTemplate);
        return agent;
    }

    @Override
    public boolean applicationExists(String appKey) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void changeAppName(String oldName, String newName) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getDeviceName() {
        // TODO Auto-generated method stub
        return name;
    }

    @Override
    public Agent getAgent() {
        // TODO Auto-generated method stub
        return null;
    }

}
