package com.incluit.automate.core.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.function.Predicate;

import org.apache.log4j.Logger;

import com.incluit.automate.core.agent.Agent;
import com.incluit.automate.core.exceptions.AgentException;

public class ProcessLogManager {
    
    private final static Logger log = Logger.getLogger(ProcessLogManager.class.getName());
    private String m_command;
    private Agent m_agent = null;
    private ArrayList<RegexItem> m_regexItemList = new ArrayList<RegexItem>();

    public ProcessLogManager(String command) {
        m_command = command;
    }
    
    public ProcessLogManager(String command, Agent agent) {
        m_command = command;
        m_agent = agent;
    }

    public void setAgent(Agent agent) {m_agent = agent;}

    /**
     * Adds a listener to the command output
     * @param regex                       The criteria to be matched.
     *
     * @param predicate                   The predicate that will get test
     *
     * @param lineNumber                  If there is more than one line that matches,
     *                                    you can specify which line (e.g: by using 3
     *                                    you are specifying that the method should be
     *                                    executed on the third match, if you want
     *                                    the function to be called on every match,
     *                                    you should use 0. If you want to start counting
     *                                    from the last to the begging, you could use
     *                                    negative numbers, being -1, the last one.
     */
    public void doOnMatch(String regex, Predicate<String> predicate, int lineNumber) {
        m_regexItemList.add(new RegexItem(regex, predicate, lineNumber));
    }

    /**
     * Executes a function if an line from the output matches a regex set on doOnMatch.
     *
     * @throws NoSuchMethodException      If the method does not exist on classWhereTheFunctionIs.
     * @throws IOException                If there is an error reading the output.
     * @throws IllegalAccessException     Might be thrown if the method is private or protected.
     * @throws IllegalArgumentException   If the method does not take a String argument (it has to be the only parameter)
     * @throws InvocationTargetException  Might be called if the method throws an exception
     * @throws AgentException             If there is an execution Exception on the command
     * @throws InstantiationException     If it could not make an instance on method.invoke(regexItem.CALLER.newInstance(), args);
     * @throws RuntimeException           If there is an execution Exception on the command
     */
    public void start() throws NoSuchMethodException, IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, AgentException, InstantiationException {
        Process p;
        if(m_agent == null) {
            p = Runtime.getRuntime().exec(m_command);
        } else {
            p = m_agent.sendCommand(m_command);
        }
        Class[] parameterTypes = new Class[1];
        parameterTypes[0] = String.class;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        String line;
        while((line = bufferedReader.readLine()) != null) {
            log.info("process out: "+line);
            for(RegexItem regexItem : m_regexItemList) {
                if(line.matches(regexItem.REGEX)) {
                    regexItem.index++;
                    if(regexItem.LINE_NUMBER > 0 && regexItem.index == regexItem.LINE_NUMBER) {
                        log.info("Executing predicate");
                        regexItem.PREDICATE.test(line);
                        return;
                    } else if(regexItem.LINE_NUMBER == 0) {
                        log.info("Executing predicate");
                        regexItem.PREDICATE.test(line);
                    } else {
                        log.info("Not executing because is not the correct line");
                    }
                }
            }
        }
        while((line = bufferedReader2.readLine()) != null) {
            log.info("process err: "+line);
            for(RegexItem regexItem : m_regexItemList) {
                if(line.matches(regexItem.REGEX)) {
                    regexItem.index++;
                    if(regexItem.LINE_NUMBER > 0 && regexItem.index == regexItem.LINE_NUMBER) {
                        log.info("Executing predicate");
                        regexItem.PREDICATE.test(line);
                        return;
                    } else if(regexItem.LINE_NUMBER == 0) {
                        log.info("Executing predicate");
                        regexItem.PREDICATE.test(line);
                    }
                }
            }
        }
    }

    public void resetRegexItems() {
        for(RegexItem regexItem : m_regexItemList)
            regexItem.index = 0;
    }
}

class RegexItem {
    public final String REGEX;
    public final Predicate<String> PREDICATE;
    public final int LINE_NUMBER;
    public int index = 0;

    RegexItem(String regex, Predicate<String> predicate, int lineNumber) {
        REGEX = regex;
        PREDICATE = predicate;
        LINE_NUMBER = lineNumber;
    }
}

