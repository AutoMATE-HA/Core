package com.incluit.automate.core.agent;

import com.incluit.automate.core.conf.Constants.AppType;
import com.incluit.automate.core.device.Device;
import com.incluit.automate.core.exceptions.AgentException;

/**
 * Manager of agents
 */
public class AgentManager {

    /**
     * Template of the agents implement package
     */
    private static final String agentImplPackageTemplate =
            "com.incluit.automate.module.%s.agent.AgentImpl";
    public static Agent lastFailedAgent;

    /**
     * Create a specific agent
     */
    public static Agent createAgent(Device device, AppType type) throws AgentException {
        try {
            return device.createAgent(type, agentImplPackageTemplate);
        } catch (Exception e) {
            throw new AgentException(e, null);
        }
    }
}
