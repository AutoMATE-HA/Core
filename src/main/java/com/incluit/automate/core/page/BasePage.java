package com.incluit.automate.core.page;

import com.incluit.automate.core.agent.Agent;
import com.incluit.automate.core.steps.StepBase;

import org.apache.log4j.Logger;

public class BasePage {

    protected final Logger log = Logger.getLogger(StepBase.class.getName());

    /**
     * Agent to use for connect to application
     */
    protected Agent agent;

    /**
     * Constructor
     *
     * @param agent Agent
     */
    public BasePage(Agent agent) {
        this.agent = agent;
    }

}
