package com.incluit.automate.core.structures;

public enum ScrollDirection {
    UP, DOWN
}
